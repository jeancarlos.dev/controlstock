<?php

class StockModel {

    private $db;
    private $stocks;
    

    public function __construct() {
        $this->db = Conectar::conexion();
        $this->stocks = array();
    }

    public function findAll() {
        $sql = 'select * from stock';
        $resultado = $this->db->query($sql);
        while ($row = $resultado->fetch_assoc()) {
            $this->stocks[] = $row;
        }
        return $this->stocks;
    }

    public function findAllBy($codigoProducto, $nombreProducto, $codigoSucursal , $nombreSucursal){
        $sql = "SELECT prod.id, prod.codigo_producto, prod.nombre, prod.categoria,suc.sucursal, suc.codigo_sucursal, st.stock_actual,st.stock_minimo,st.stock_maximo,st.precio
                        FROM stock st
                        INNER JOIN productos prod ON prod.codigo_producto = st.codigo_producto
                        INNER JOIN sucursales suc ON suc.codigo_sucursal = st.codigo_sucursal WHERE (prod.codigo_producto='$codigoProducto' OR prod.nombre like '%$nombreProducto%') OR
                        (suc.codigo_sucursal='$codigoSucursal' OR suc.sucursal like '%$nombreSucursal%')";



                           $resultado = $this->db->query($sql);
                                while ($row = $resultado->fetch_assoc()) {
                                    $this->stocks[] = $row;
                                }
                                return $this->stocks;

    }

    public function findAllDetail() {
        $sql = 'SELECT prod.id, prod.codigo_producto, prod.nombre, prod.categoria,suc.sucursal, suc.codigo_sucursal, st.stock_actual,st.stock_minimo,st.stock_maximo,st.precio
FROM stock st
INNER JOIN productos prod ON prod.codigo_producto = st.codigo_producto
INNER JOIN sucursales suc ON suc.codigo_sucursal = st.codigo_sucursal';
        $resultado = $this->db->query($sql);
        while ($row = $resultado->fetch_assoc()) {
            $this->stocks[] = $row;
        }
        return $this->stocks;
    }
    
    public function findBy($cmbFiltro, $busqueda) {
        
        $filtroFinal;
        
        if($cmbFiltro=='codigo_producto' || $cmbFiltro=='nombre' ){
            $filtroFinal='prod.'.$cmbFiltro;
        }
        
         if($cmbFiltro=='codigo_sucursal' || $cmbFiltro=='sucursal' ){
            $filtroFinal='suc.'.$cmbFiltro;
        }
        
        
        
         $sql = "SELECT prod.id, prod.codigo_producto, prod.nombre, prod.categoria,suc.sucursal, suc.codigo_sucursal, st.stock_actual,st.stock_minimo,st.stock_maximo,st.precio
FROM stock st
INNER JOIN productos prod ON prod.codigo_producto = st.codigo_producto
INNER JOIN sucursales suc ON suc.codigo_sucursal = st.codigo_sucursal WHERE $filtroFinal LIKE '%$busqueda%' ";
         
        
        
        $resultado = $this->db->query($sql);
            while ($row = $resultado->fetch_assoc()) {
            $this->stocks[] = $row;
        }
        return $this->stocks;
    }

    public function findById($codigo_sucursal, $codigo_producto) {
        $sql = "SELECT * FROM stock WHERE codigo_sucursal='$codigo_sucursal' and codigo_producto='$codigo_producto' LIMIT 1";
        $resultado = $this->db->query($sql);
        $row = $resultado->fetch_assoc();
        return $row;
    }

    public function insertar($codigoProducto, $codigoSucursal, $stockActual, $stockMinimo, $stockMaximo, $precio) {
        $sql = "INSERT INTO stock (codigo_producto,codigo_sucursal,stock_actual,stock_minimo, stock_maximo,precio) "
                . "VALUES ('$codigoProducto', '$codigoSucursal', '$stockActual', '$stockMinimo', '$stockMaximo','$precio')";
        $resultado = $this->db->query($sql);
    }

    public function update($codigo_sucursal, $codigo_producto, $stock_actual, $stock_minimo, $stock_maximo, $precio) {
        $sql = "UPDATE stock SET stock_actual='$stock_actual' ,stock_minimo='$stock_minimo' ,stock_maximo='$stock_maximo' ,precio='$precio' WHERE codigo_sucursal='$codigo_sucursal' AND codigo_producto='$codigo_producto';";
        $resultado = $this->db->query($sql);
    }

    public function delete($codigo_sucursal, $codigo_producto) {
        $sql = "DELETE FROM stock WHERE codigo_sucursal='$codigo_sucursal' AND codigo_producto='$codigo_producto';";
        $resultado = $this->db->query($sql);
    }

}
