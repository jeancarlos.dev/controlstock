create table usuarios(
id                  int AUTO_INCREMENT PRIMARY KEY,
email               varchar(50) not null unique,
clave               varchar(10) not null,
activo              boolean default true
);

create table sucursales(
id int                  AUTO_INCREMENT PRIMARY KEY,
codigo_sucursal         varchar(10) not null unique,
sucursal                varchar(100)not null unique,
eliminado               boolean     default false
);

create table productos(
id                      int AUTO_INCREMENT PRIMARY KEY,
codigo_producto         varchar(10) not null unique,
nombre                  varchar(50) not null unique,
categoria               varchar(20) not null,
descripcion             text,
eliminado               boolean     default false
);

create table stock(
codigo_producto         varchar(10),
codigo_sucursal         varchar(10),
stock_actual            int not null,
stock_minimo            int not null,
stock_maximo            int not null,
precio                  int not null,
CONSTRAINT codigo_producto_FK FOREIGN KEY (codigo_producto) REFERENCES productos(codigo_producto),
CONSTRAINT codigo_sucursal_FK FOREIGN KEY (codigo_sucursal) REFERENCES sucursales(codigo_sucursal),
primary key (codigo_producto, codigo_sucursal)
);

/*Inserciones de productos*/
-- insert into productos (nombre,codigo_producto,categoria,descripcion)values('procesador ryzen 9 3900x','prod501','tecnologia','producto de alta gama');
-- insert into productos (nombre,codigo_producto,categoria,descripcion)values('ram gskill 8GB','prod502','tecnologia','producto de alta gama');
-- insert into productos (nombre,codigo_producto,categoria,descripcion)values('disco ssd Kinstong 1TB','prod503','tecnologia','producto de alta gama');
-- insert into productos (nombre,codigo_producto,categoria,descripcion)values('tarjeta de video RTX 2090','prod504','tecnologia','producto de alta gama');
-- insert into productos (nombre,codigo_producto,categoria,descripcion)values('teclado logitech master key MX','prod505','tecnologia','producto de alta gama');

/*Inserciones de sucursales*/
-- insert into sucursales(codigo_sucursal,sucursal)values('suc501','concepcion');
-- insert into sucursales(codigo_sucursal,sucursal)values('suc502','metropolitana');
-- insert into sucursales(codigo_sucursal,sucursal)values('suc503','maule');
-- insert into sucursales(codigo_sucursal,sucursal)values('suc504','iquique');
-- insert into sucursales(codigo_sucursal,sucursal)values('suc505','puerto montt');

/*Inserciones de stock*/
-- insert into stock(codigo_producto,codigo_sucursal,stock_actual,stock_minimo,stock_maximo,precio)values('prod501','suc501',10,1,500,24000);
-- insert into stock(codigo_producto,codigo_sucursal,stock_actual,stock_minimo,stock_maximo,precio)values('prod501','suc502',100,1,500,22000);
-- insert into stock(codigo_producto,codigo_sucursal,stock_actual,stock_minimo,stock_maximo,precio)values('prod501','suc503',1,1,500,20000);
-- 
-- insert into stock(codigo_producto,codigo_sucursal,stock_actual,stock_minimo,stock_maximo,precio)values('prod502','suc501',10,1,500,53000);
-- insert into stock(codigo_producto,codigo_sucursal,stock_actual,stock_minimo,stock_maximo,precio)values('prod502','suc502',100,1,500,45000);
-- insert into stock(codigo_producto,codigo_sucursal,stock_actual,stock_minimo,stock_maximo,precio)values('prod502','suc503',25,1,500,50000);
-- insert into stock(codigo_producto,codigo_sucursal,stock_actual,stock_minimo,stock_maximo,precio)values('prod502','suc504',56,1,500,49990);
-- insert into stock(codigo_producto,codigo_sucursal,stock_actual,stock_minimo,stock_maximo,precio)values('prod502','suc505',88,1,500,52990);
-- 
-- insert into stock(codigo_producto,codigo_sucursal,stock_actual,stock_minimo,stock_maximo,precio)values('prod503','suc503',10,1,500,150000);
-- insert into stock(codigo_producto,codigo_sucursal,stock_actual,stock_minimo,stock_maximo,precio)values('prod503','suc504',100,1,500,149990);
-- insert into stock(codigo_producto,codigo_sucursal,stock_actual,stock_minimo,stock_maximo,precio)values('prod503','suc505',25,1,500,139000);
-- 
-- insert into stock(codigo_producto,codigo_sucursal,stock_actual,stock_minimo,stock_maximo,precio)values('prod504','suc505',10,1,500,9990);
-- 
-- insert into stock(codigo_producto,codigo_sucursal,stock_actual,stock_minimo,stock_maximo,precio)values('prod505','suc503',10,1,500,450000);
-- insert into stock(codigo_producto,codigo_sucursal,stock_actual,stock_minimo,stock_maximo,precio)values('prod505','suc504',10,1,500,439990);


insert into usuarios(email, clave) values('jfaundez@ciisa.cl','ciisa123');
insert into usuarios(email, clave) values('fespinoza@ciisa.cl','ciisa456');
insert into usuarios(email, clave) values('dgalvez@ciisa.cl','ciisa789');

/*Para consulta personalizada*/
SELECT prod.id, prod.codigo_producto, prod.nombre, prod.categoria,suc.sucursal, suc.codigo_sucursal, st.stock_actual,st.precio
FROM stock st
INNER JOIN productos prod ON prod.codigo_producto = st.codigo_producto
INNER JOIN sucursales suc ON suc.codigo_sucursal = st.codigo_sucursal WHERE st.codigo_sucursal = parametro_sucursal OR parametro_sucursal= ''


SELECT prod.id, prod.codigo_producto, prod.nombre, prod.categoria,suc.sucursal, suc.codigo_sucursal, st.stock_actual,st.precio
FROM stock st
INNER JOIN productos prod ON prod.codigo_producto = st.codigo_producto
INNER JOIN sucursales suc ON suc.codigo_sucursal = st.codigo_sucursal