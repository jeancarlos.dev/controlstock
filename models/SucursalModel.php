<?php

class SucursalModel{
    
    private $db;
    private $sucursales;

    public function __construct() {
        $this->db = Conectar::conexion();
        $this->sucursales = array();
    }
   
    public function findAll() {
        
        $sql = 'select * from sucursales where eliminado=0';
        $resultado = $this->db->query($sql);
        while ($row = $resultado->fetch_assoc()) {
            $this->sucursales[] = $row;
        }
        return $this->sucursales;
    }

    public function findById($id) {
        $sql = "SELECT * FROM sucursales WHERE id='$id' LIMIT 1";
        $resultado = $this->db->query($sql);
        $row = $resultado->fetch_assoc();
        return $row;
    }
    
      public function findBy($cmbFiltro, $busqueda) {
        $sql = "SELECT * FROM sucursales WHERE $cmbFiltro LIKE '%$busqueda%' and eliminado=0";
        $resultado = $this->db->query($sql);
            while ($row = $resultado->fetch_assoc()) {
            $this->sucursales[] = $row;
        }
        return $this->sucursales;
    }

    public function insertar($codigo, $sucursal) {
        $sql="INSERT INTO sucursales (codigo_sucursal,sucursal) VALUES ('$codigo', '$sucursal')";
        $resultado = $this->db->query($sql);
    }

    public function update($id, $codigo, $sucursal) {
        $sql="UPDATE sucursales SET codigo_sucursal='$codigo', sucursal='$sucursal' WHERE id = '$id'";
        $resultado = $this->db->query($sql);
    }
    
     public function updateSucursal($codigo_sucursal) {
        $sql="UPDATE sucursales SET codigo_sucursal='$codigo_sucursal' WHERE codigo_sucursal= '$codigo_sucursal'";
        $resultado = $this->db->query($sql);
    }

    public function delete($id) {
        $resultado = $this->db->query("UPDATE sucursales SET eliminado=1 WHERE id = '$id'");
    }
    
}

