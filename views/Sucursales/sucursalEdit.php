<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Control de stock</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" >
        <link href="assets/css/style.css" rel="stylesheet" >


    </head>
    <body>



        <section class="container gap20" id="container">




            <div class="card bg">
                <div class="card-header">





                    <h3><?php echo $data["Titulo"]; ?> </h3>





                </div>
                <div class="card-body">

                    <form id="nuevo" name="nuevo" method="POST" action="index.php?c=sucursal&a=actualizar" autocomplete="off">
                        <input type="hidden" id="id" name="id" value="<?php echo $data["id"]; ?>" />
                        <div class="mb-3">
                            <label class="form-label">Código de sucursal</label>
                            <input type="text" class="form-control" id="codigo" name="codigo_sucursal" placeholder="Ej: SUC01" value="<?php echo $data["sucursales"]["codigo_sucursal"] ?>" required/>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Nombre</label>
                            <input type="text" class="form-control" id="sucursal" name="sucursal" placeholder="EJ: Concepción" value="<?php echo $data["sucursales"]["sucursal"] ?>" required/>
                        </div>



                        <div class="fx gap-right">

                            <a id="atras" name="atras" class="btn btn-secondary" href="index.php?c=sucursal&a=index" >Atrás</a>

                            <button id="guardar" name="guardar" class="btn btn-primary" type="submit">Modificar sucursal</button>

                        </div>  

                    </form>




                </div>
            </div>

        </section>




    </body>
    <script src="assets/js/bootstrap.bundle.min.js" ></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
</html>
