<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * ProductModel descripcion
 *
 * @author Jean Carlos
 */
class ProductModel {

    private $db;
    private $productos;

    public function __construct() {
        $this->db = Conectar::conexion();
        $this->productos = array();
    }

    public function findAll() {
        $sql = 'select * from productos where eliminado=0';
        $resultado = $this->db->query($sql);
        while ($row = $resultado->fetch_assoc()) {
            $this->productos[] = $row;
        }
        return $this->productos;
    }

    public function findBy($cmbFiltro, $busqueda) {
        $sql = "SELECT * FROM productos WHERE $cmbFiltro LIKE '%$busqueda%' and eliminado=0";
        $resultado = $this->db->query($sql);
            while ($row = $resultado->fetch_assoc()) {
            $this->productos[] = $row;
        }
        return $this->productos;
    }
    
     public function findByCodigoAndNombre($cmbFiltro, $busqueda) {
        $sql = "SELECT * FROM productos WHERE $cmbFiltro LIKE '%$busqueda%' and eliminado=0";
        $resultado = $this->db->query($sql);
            while ($row = $resultado->fetch_assoc()) {
            $this->productos[] = $row;
        }
        return $this->productos;
    }
    
    
    
    
    
    

    public function findAllDetail() {
        $sql = "SELECT prod.id, prod.codigo_producto, prod.nombre, prod.categoria, suc.codigo_sucursal,suc.sucursal, st.stock_actual,st.stock_minimo, st.stock_maximo, st.precio
from stock st
INNER JOIN productos prod ON prod.codigo_producto = st.codigo_producto
INNER JOIN sucursales suc ON suc.codigo_sucursal = st.codigo_sucursal";
        $resultado = $this->db->query($sql);
        while ($row = $resultado->fetch_assoc()) {
            $this->productos[] = $row;
        }

        return $this->productos;
    }

    public function findById($id) {
        $sql = "SELECT * FROM productos WHERE id='$id' LIMIT 1";
        $resultado = $this->db->query($sql);
        $row = $resultado->fetch_assoc();
        return $row;
    }
    
     public function findByCodigoProducto($codigo_producto) {
        $sql = "SELECT * FROM productos WHERE codigo_producto='$codigo_producto'";
        $resultado = $this->db->query($sql);
        $row = mysqli_num_rows($resultado);
        return $row;
    }
    
         public function findByNombreProducto($nombre) {
        $sql = "SELECT * FROM productos WHERE nombre='$nombre'";
        $resultado = $this->db->query($sql);
        $row = mysqli_num_rows($resultado);
        return $row;
    }
    
    

    public function insertar($codigo, $nombre, $categoria, $descripcion) {
        $sql = "INSERT INTO productos (codigo_producto,nombre,categoria,descripcion) VALUES ('$codigo', '$nombre', '$categoria', '$descripcion')";
        $resultado = $this->db->query($sql);
    }

    public function update($id, $codigo, $nombre, $categoria, $descripcion) { 
        $sql = "UPDATE productos SET codigo_producto='$codigo', nombre='$nombre', categoria='$categoria', descripcion='$descripcion' WHERE id='$id'";
        $resultado = $this->db->query($sql);
    }
    
      public function updateDatos($id, $nombre, $categoria, $descripcion) { 
        $sql = "UPDATE productos SET nombre='$nombre', categoria='$categoria', descripcion='$descripcion' WHERE id='$id'";
        $resultado = $this->db->query($sql);
    }

    public function delete($id) {
        $resultado = $this->db->query("UPDATE productos SET eliminado=1 WHERE id= '$id'");
    }

}

?>