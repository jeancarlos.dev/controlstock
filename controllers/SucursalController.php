<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class SucursalController{
    
    public function __construct() {
        require_once "models/SucursalModel.php";
    }
    
     public function index(){
      
        $sucursales = new SucursalModel();
        $data["Titulo"] = "Administración de sucursales";
        $data["sucursales"] = $sucursales->findAll();
        require_once "views/Sucursales/sucursalAdmin.php";
    } 
    
    public function busqueda() {
        $txtBusqueda = $_POST['txtBusqueda'];
        $cmbFiltro = $_POST['cmbFiltro'];

        $sucursales = new SucursalModel();


        //Si no hay un filtro seleccionado y tampoco una busqueda entonces trae todos los productos
        if ($cmbFiltro == '' && $txtBusqueda == null || $cmbFiltro != '' && $txtBusqueda == null) {
            $data["sucursales"] = $sucursales->findAll();
            $data["Titulo"] = "Administracion de sucursales";
            require_once "views/Sucursales/sucursalAdmin.php";
        }

        //Si el filtro no esta seleccionado y tiene escrito una busqueda, por defecto buscara por nombre de producto 
        if ($cmbFiltro == '' && $txtBusqueda != null) {
            $data["sucursales"] = $sucursales->findBy('sucursal', $txtBusqueda);
            $data["Titulo"] = "Administracion de sucursales";
             require_once "views/Sucursales/sucursalAdmin.php";
        }

        //Si el filtro esta seleccionado y tiene escrita una busqueda realizara el filtro
        if ($cmbFiltro != '' && $txtBusqueda != null) {
            $data["sucursales"] = $sucursales->findBy($cmbFiltro, $txtBusqueda);
            $data["Titulo"] = "Administracion de productos";
             require_once "views/Sucursales/sucursalAdmin.php";
        }
    }
    
    public function nuevo(){
         $data["Titulo"] = "Nueva sucursal";
         require_once "views/Sucursales/sucursalNew.php";
    }
    
    public function guarda() {
        $codigo = $_POST['codigo_sucursal'];
        $nombre = $_POST['sucursal'];
  
        $sucursales = new SucursalModel();
        $sucursales->insertar($codigo, $nombre);
       
        $data["titulo"] = "Administración de sucursales";
        $this->index();
    }
    
    public function modificar($id) {
        $sucursales = new SucursalModel();
        $data["id"] = $id;
        $data["sucursales"] = $sucursales->findById($id);
        $data["Titulo"] = "Modificar sucursal";
         require_once "views/Sucursales/sucursalEdit.php";
    }
    
    
    
    
    
    public function actualizar() {
        
      
        $id = $_POST['id'];
        $codigo = $_POST['codigo_sucursal'];
        $sucursal = $_POST['sucursal'];

        $sucursales = new SucursalModel();
        $sucursales->update($id, $codigo, $sucursal);
        $data["titulo"] = "Administracion de sucursales";
        $this->index();
    }
   
    
    
    
    
    public function eliminar($id) {
        require_once "models/SucursalModel.php";
        $sucursales = new SucursalModel();
        $sucursales->delete($id);
        $data["titulo"] = "Administración de sucursales";
        $this->index();
    }
    
}
