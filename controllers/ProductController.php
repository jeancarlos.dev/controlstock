<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of myUsuariosControlador
 *
 * @author Jean Carlos
 */
class ProductController {

    public function __construct() {
        require_once "models/ProductModel.php";
    }

    public function busqueda() {
        $txtBusqueda = $_POST['txtBusqueda'];
        $cmbFiltro = $_POST['cmbFiltro'];

        $productos = new ProductModel();



        //Si no hay un filtro seleccionado y tampoco una busqueda entonces trae todos los productos
        if ($cmbFiltro == '' && $txtBusqueda == null || $cmbFiltro != '' && $txtBusqueda == null) {
            $data["productos"] = $productos->findAll();
            $data["Titulo"] = "Administracion de productos";
            require_once "views/Products/ProductAdmin.php";
        }

        //Si el filtro no esta seleccionado y tiene escrito una busqueda, por defecto buscara por nombre de producto 
        if ($cmbFiltro == '' && $txtBusqueda != null) {
            $data["productos"] = $productos->findBy('nombre', $txtBusqueda);
            $data["Titulo"] = "Administracion de productos";
            require_once "views/Products/ProductAdmin.php";
        }

        //Si el filtro esta seleccionado y tiene escrita una busqueda realizara el filtro
        if ($cmbFiltro != '' && $txtBusqueda != null) {
            $data["productos"] = $productos->findBy($cmbFiltro, $txtBusqueda);
            $data["Titulo"] = "Administracion de productos";
            require_once "views/Products/ProductAdmin.php";
        }
    }

    public function busquedaAvanzada() {
        $filtroCodigo = $_POST['codigo_producto'];
        $filtroNombre = $_POST['nombre'];

        $arrayFiltros = array($filtroCodigo, $filtroNombre);

        $filtros = array_map("codigo_producto", $filtroCodigo);
    }

    public function index() {

        session_start();

        echo "VALIDANDO" . $_SESSION['miSesion'];
        if (!isset($_SESSION["miSesion"])) {
            header("location:index.php?c=user&a=index");
        }else{
            
        $productos = new ProductModel();
        $data["Titulo"] = "Administración de productos";
        $data["productos"] = $productos->findAll();
        require_once "views/Products/ProductAdmin.php";
            
        }

      
    }

    public function nuevo() {
        $data["Titulo"] = "Nuevo producto";
        require_once "views/Products/ProductNew.php";
    }

    public function guarda() {
        $codigo_producto = $_POST['codigo_producto'];
        $nombre = $_POST['nombre'];
        $categoria = $_POST['categoria'];
        $descripcion = $_POST['descripcion'];
        $productos = new ProductModel();
        $productos->insertar($codigo_producto, $nombre, $categoria, $descripcion);
        $data["titulo"] = "Administración de productos";
        $this->index();
    }

    public function modificar($id) {
        $productos = new ProductModel();
        $data["id"] = $id;
        $data["productos"] = $productos->findById($id);
        $data["Titulo"] = "Modificar producto";
        require_once "views/Products/ProductEdit.php";
    }

    public function validarDatos() {
        $codigo_producto = $_POST['codigo_producto'];
        $nombre = $_POST['nombre'];

        $productos = new ProductModel();
        $encontradoCodigo = $productos->findByCodigoProducto($codigo_producto);
        $encontradoNombre = $productos->findByNombreProducto($nombre);


        if ($encontradoCodigo == 1) {
            echo "Codigo producto: " . $codigo_producto . " ya existe <br>";
            return false;
        }

        if ($encontradoNombre == 1) {
            echo "Nombre producto: " . $nombre . "  ya existe: <br>";
            return false;
        }

        return true;
    }

    public function actualizar() {
        //1-se obtienen los parametros para buscar el producto
        $id = $_POST['id'];
        $codigo_producto = $_POST['codigo_producto'];
        $nombre = $_POST['nombre'];
        $categoria = $_POST['categoria'];
        $descripcion = $_POST['descripcion'];


        //2-busca el producto en base de datos por CODIGO_PRODUCTO
        $productos = new ProductModel();
        $encontradoCodigo = $productos->findByCodigoProducto($codigo_producto);



        if ($encontradoCodigo == 1) {
            echo "Codigo producto: " . $codigo_producto . " ya existe <br>";
            echo "Solo se actualizaran los no obligatorios (categoria, descripcion)";


            $productos = new ProductModel();
            $productos->updateDatos($id, $nombre, $categoria, $descripcion);
            $data["titulo"] = "Administracion de productos";
            $this->index();
        }




        //Si no existe el producto en base de datos, entonces permite guardar
        if ($encontradoCodigo == 0) {
            $productos = new ProductModel();
            $productos->update($id, $codigo_producto, $nombre, $categoria, $descripcion);
            $data["titulo"] = "Administracion de productos";
            $this->index();
        }
    }

    public function eliminar($id) {
        require_once "models/ProductModel.php";
        $productos = new ProductModel();
        $productos->delete($id);

        $data["titulo"] = "Administracion de productos";
        $this->index();
    }

}
