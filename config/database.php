<?php
/**
 * @author Jean Carlos
 */
class Conectar {

    public static function conexion() {

        $host = "127.0.0.1";
        $username = "root";
        $bdname = "evaluacion1";
        $pass = "";
        $port="3307";

        $conexion = new mysqli($host, $username, $pass, $bdname, $port);

        if ($conexion->connect_error) {
            echo "No conectado";
            die('Connect Error (' . $conexion->connect_errno . ') ' . $conexion->connect_error);
        } else {
            //echo "Conectado";
            return $conexion;
        }
    }

}
