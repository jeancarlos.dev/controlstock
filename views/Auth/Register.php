<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Control de stock</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" >
        <link href="assets/css/style.css" rel="stylesheet" >


    </head>
    <body>

 

        <section class="container d-flex justify-content-center align-items-center" style="height:100vh;">




            <div class="card bg">
                <div class="card-header">
                    <h3><?php echo $data["Titulo"]; ?> </h3>
                </div>
                <div class="card-body">

                    <form id="nuevo" name="nuevo" method="POST" action="index.php?c=user&a=registrarUsuario" autocomplete="off">

                        <div class="mb-3">
                            <label class="form-label">Correo</label>
                            <input type="text" class="form-control" id="correo" name="correo" placeholder="Ej: jfaundez@ciisa.cl" required>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Contraseña</label>
                            <input type="password" class="form-control" id="clave" name="clave" placeholder="Ingrese su contraseña" required>
                        </div>

                        <div class="fx gap-right">

                            <button id="atras" name="atras" class="btn btn-secondary" onclick="window.location.href = 'index.php?c=user&a=index'" >Atrás</a>
                            <button id="guardar" name="guardar" class="btn btn-primary" type="submit">Registrar</button>


                        </div>  

                    </form>




                </div>
            </div>

        </section>

        <script src="assets/js/bootstrap.bundle.min.js" ></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>


    </body>

</html>
