<?php




class DashboardController{
    
  
    public function index(){
        $data["Titulo"] = "Dashboard";
        require_once 'views/dashboard/dashboard.php';
    }
    
    public function cerrarSesion(){
     
        session_destroy();
        
        require_once 'views/Auth/login.php';
    }
    
    
    
}
