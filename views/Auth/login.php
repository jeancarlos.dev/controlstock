<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <!--<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">-->

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" >
        <link href="assets/css/style.css" rel="stylesheet" >
    </head>

    <body>

    <!--<img src="src/resources/img/usuario.png" alt="">-->


        <div class="container d-flex justify-content-center align-items-center" style="height:100vh;">


            <div class="card">
                <div class="card-body">

                    <!-- <form> -->
                    <fieldset>
                        <legend>Acceso a Gestion de Stock</legend>

                        <form method="POST" action="index.php?c=user&a=autenticar">
                            <div class="mb-3">
                                <label for="disabledTextInput" class="form-label">Usuario</label>
                                <input type="text" id="email" name="email" class="form-control" placeholder="Ingrese su email" required>
                            </div>
                            <div class="mb-3">
                                <label for="disabledTextInput" class="form-label">Constraseña</label>

                                <input type="password" id="clave" name="clave" pattern="[A-Za-z0-9_-]{1,15}" placeholder="Ingrese su contraseña" class="form-control" aria-describedby="passwordHelpInline" required>
                            </div>

                            <div class="d-flex column">
                                <button type="submit" class="btn btn-primary" role="link" >Iniciar sesión</button>
                               
                            </div>

                        </form>
                        
                         <p style="margin-top:20px">¿No tienes cuenta aun? <a href="index.php?c=user&a=indexRegistrar">Haz click aquí</a></p>





                    </fieldset>

                    <!-- </form> -->
                    <!-- Se crea un boton para redireccionar a la administracion, este es solo temporal ya que despues sera reemplazado dentro de un formulario para realizar la primera peticion post. -->
                    <!-- <button type="submit" class="btn btn-primary" role="link" onclick="window.location='/dashboard'"  >Iniciar sesión</button> -->
                </div>


                <!--                <div class="alert alert-danger" role="alert">
                                    Usuario no encontrado
                                    
                                </div>-->



            </div>


        </div>


    </body>
    <script src="assets/js/bootstrap.bundle.min.js" ></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

</html>
