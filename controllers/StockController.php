<?php

class StockController {

    public function __construct() {
        require_once "models/StockModel.php";
    }

    public function index() {
        $stocks = new StockModel();
        $data["Titulo"] = "Administración de stock";
        $data["stocks"] = $stocks->findAllDetail();
 
        require_once "views/Stock/stockAdmin.php";
    }
    
    public function busqueda() {
        $txtBusqueda = $_POST['txtBusqueda'];
        $cmbFiltro = $_POST['cmbFiltro'];
        $stocks = new StockModel();

        //Si no hay un filtro seleccionado y tampoco una busqueda entonces trae todos los productos
        if ($cmbFiltro == '' && $txtBusqueda == null || $cmbFiltro != '' && $txtBusqueda == null) {
            $data["stocks"] = $stocks->findAllDetail();
            $data["Titulo"] = "Administracion de stock";
            require_once "views/Stock/stockAdmin.php";
        }

        //Si el filtro no esta seleccionado y tiene escrito una busqueda, por defecto buscara por nombre de producto 
        if ($cmbFiltro == '' && $txtBusqueda != null) {
            $data["stocks"] = $stocks->findBy('nombre', $txtBusqueda);
            $data["Titulo"] = "Administracion de stock";
            require_once "views/Stock/stockAdmin.php";
        }

        //Si el filtro esta seleccionado y tiene escrita una busqueda realizara el filtro
        if ($cmbFiltro != '' && $txtBusqueda != null) {
            $data["stocks"] = $stocks->findBy($cmbFiltro, $txtBusqueda);
            $data["Titulo"] = "Administracion de stock";
            require_once "views/Stock/stockAdmin.php";
        }
    }

    public function busquedaAvanzada(){
               $codigoProducto = $_POST['txtCodigoProducto'];
               $nombreProducto = $_POST['txtNombreProducto'];
               $codigoSucursal = $_POST['txtCodigoSucursal'];
               $nombreSucursal = $_POST['txtNombreSucursal'];


                 echo "Codigo de producto: ".$codigoProducto;
                           echo "Nombre de producto: ".$nombreProducto;
                           echo "Codigo de sucursal: ".$codigoSucursal;
                           echo "Nombre de sucursal: ".$nombreSucursal;

               $stocks = new StockModel();

               $data["stocks"] = $stocks->findAllBy($codigoProducto, $nombreProducto,$codigoSucursal,$nombreSucursal );
$data["Titulo"] = "Administracion de stock";
            require_once "views/Stock/stockAdmin.php";



    }

    public function nuevo() {
        $data["Titulo"] = "Nuevo stock";
        require_once "views/Stock/stockNew.php";
    }

    public function guarda() {
        $codigo_sucursal = $_POST['codigo_sucursal'];
        $codigo_producto = $_POST['codigo_producto'];
        $stock_actual = $_POST['stock_actual'];
        $stock_minimo = $_POST['stock_minimo'];
        $stock_maximo = $_POST['stock_maximo'];
        $precio = $_POST['precio'];

 

        $stocks = new StockModel();
        $stocks->insertar($codigo_producto, $codigo_sucursal, $stock_actual, $stock_minimo, $stock_maximo, $precio);
        $data["titulo"] = "Administración de stock";
        $this->index();
    }

    public function modificar() {
        $codigo_sucursal = $_GET['codigo_sucursal'];
        $codigo_producto = $_GET['codigo_producto'];
        
        $stocks = new StockModel();

        $data["stocks"] = $stocks->findById($codigo_sucursal,$codigo_producto);
        $data["Titulo"] = "Modificar stock";
        require_once "views/Stock/stockEdit.php";
    }

    public function actualizar() {

        $codigo_sucursal = $_POST['codigo_sucursal'];
        $codigo_producto = $_POST['codigo_producto'];
        $stock_actual = $_POST['stock_actual'];
        $stock_minimo = $_POST['stock_minimo'];
        $stock_maximo = $_POST['stock_maximo'];
        $precio = $_POST['precio'];

        $stocks = new StockModel();
        $stocks->update($codigo_sucursal, $codigo_producto, $stock_actual, $stock_minimo,$stock_maximo, $precio);

        $data["titulo"] = "Administracion de stock";
        $this->index();
    }

    public function eliminar() {
        $codigo_sucursal = $_GET['codigo_sucursal'];
        $codigo_producto = $_GET['codigo_producto']; 
        $stocks = new StockModel();
        $stocks->delete($codigo_sucursal, $codigo_producto);
        $data["titulo"] = "Administracion de stock";
        $this->index();
    }

}
