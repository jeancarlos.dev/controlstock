<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Control de stock</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" >
        <link href="assets/css/style.css" rel="stylesheet" >



    </head>
    <body>
        <section class="container" id="container">

            <div class="fx space-between gap20">
                <div>
                    <button id="atras" name="Dashboard" class="btn btn-light" onclick="window.location.href = 'index.php?c=dashboard&a=index'" >Dashboard</a>  
                </div>
                <h3><?php echo $data["Titulo"]; ?> </h3>

                <div class="fx align-items">
                    <a class="btn btn-primary" href="index.php?c=stock&a=nuevo">Agregar stock</a>
                </div>

            </div>

            <form method="POST" action="index.php?c=stock&a=busqueda" >


                <div style="display: flex;justify-content: space-between;">
                    <div style="width: 316px;margin-right: 20px;">
                        <select class="form-select" aria-label="Default select example" id="cmbFiltro" name="cmbFiltro">
                            <option value="" >Filtro</option>
                            <option value="codigo_sucursal">Código de sucursal</option>
                            <option value="sucursal">Nombre de sucursal</option>
                            
                            <option  value="codigo_producto">Código de producto</option>
                            <option selected value="nombre">Nombre de producto</option>
                           

                        </select>
                    </div>

                    <!--Busqueda básica (1 solo filtro)-->
                    <div id="btnBasico" style="margin-right: 20px;display: flex;width: 95%;"> 
                        <input style="margin-right: 20px;" type="text" class="form-control" id="txtBusqueda" name="txtBusqueda" placeholder="Búsqueda básica" >
                        <button id="guardar" name="guardar" class="btn btn-primary" type="submit">Búsqueda</button>
                    </div>

                    <div id="btnAvanzado" style="width: 255px;">
                        <button class="btn btn-secondary" type="button" data-bs-toggle="collapse" data-bs-target=".multi-collapse1" aria-expanded="false" aria-controls="">Busqueda avanzada</button>
                    </div>
                </div>

            </form>


                <!--Busqueda Avanzada (varios filtros)-->

             <form method="POST" action="index.php?c=stock&a=busquedaAvanzada" >
                <div class="collapse multi-collapse1" id="multiCollapseExample1" style="margin-top: 20px">



                    <div class="card">
                        <div class="card-header">
                            Busqueda avanzada, seleccione un filtro o varios
                        </div>


                        <div class="card-body " >



                            <div class="contenedor-busqueda-avanzada" style="justify-content: space-between">
                                <div class="filtro-avanzado-stock" >
                                <div class="form-check form-switch" style="margin-right: 20px" >
                                    <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault">
                                    <label class="form-check-label" for="flexSwitchCheckDefault1">Código de producto</label>
                                </div>
                                <input type="text" class="form-control" id="txtCodigoProducto" name="txtCodigoProducto" placeholder="Código del producto" >
                            </div>

                            <div class="filtro-avanzado-stock">
                                <div class="form-check form-switch" style="margin-right: 20px" >
                                    <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault">
                                    <label class="form-check-label" for="flexSwitchCheckDefault1">Nombre de producto</label>
                                </div>
                                <input type="text" class="form-control" id="txtNombreProducto" name="txtNombreProducto" placeholder="Nombre del producto" >
                            </div>
                            <div class="filtro-avanzado-stock" >
                                <div class="form-check form-switch" style="margin-right: 20px" >
                                    <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault">
                                    <label class="form-check-label" for="flexSwitchCheckDefault1">Código de sucursal</label>
                                </div>
                                <input type="text" class="form-control" id="txtCodigoSucursal" name="txtCodigoSucursal" placeholder="Código de sucursal" >
                            </div>

                            <div class="filtro-avanzado-stock">
                                <div class="form-check form-switch" style="margin-right: 20px" >
                                    <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault">
                                    <label class="form-check-label" for="flexSwitchCheckDefault1">Nombre de sucursal</label>
                                </div>
                                <input type="text" class="form-control" id="txtNombreSucursal" name="txtNombreSucursal" placeholder="Nombre de sucursal" >
                            </div>
                            </div>


                            
                            <div style="display:flex;justify-content: center;margin-top:20px;">
                                <button id="guardar" name="busquedaAvanzada" class="btn btn-primary" type="submit">Búsqueda avanzada</button>
                            </div>
                            
                          
                            
                        </div>
                        
                        
                          



                    </div>

                </div>
            </form>





            <table class="table table table-striped" style="margin-top: 20px;">
                <thead>
                    <tr>

                        <th>Código producto</th>
                        <th>Nombre de producto</th>
                        <th>Categoria</th>
                        <th>Sucursal</th>
                        <th>Código de sucursal</th>
                        <th>Stock actual</th>
                        <th>Stock mínimo</th>
                        <th>Stock máximo</th>
                        <th>Precio</th>
                        
                        <th>Modificar</th>
                        <th>Eliminar</th>

                    </tr>
                </thead>
                <tbody>

                    <?php
                    foreach ($data["stocks"] as $fila) {
                        echo "<tr>";

                        echo "<td>" . $fila["codigo_producto"] . "</td>"; 
                        echo "<td>" . $fila["nombre"] . "</td>";
                        echo "<td>" . $fila["categoria"] . "</td>";
                        echo "<td>" . $fila["sucursal"] . "</td>";
                        echo "<td>" . $fila["codigo_sucursal"] . "</td>";
                        echo "<td>" . $fila["stock_actual"] . "</td>";
                        echo "<td>" . $fila["stock_minimo"] . "</td>";
                        echo "<td>" . $fila["stock_maximo"] . "</td>";
                        echo "<td>" . $fila["precio"] . "</td>";
              

                        echo "<td><a href='index.php?c=stock&a=modificar&codigo_sucursal=" . $fila["codigo_sucursal"] . "&codigo_producto=" . $fila["codigo_producto"] . "' class='btn btn-warning btn-sm'>Modificar</a></td>";
                        echo "<td><a href='index.php?c=stock&a=eliminar&codigo_sucursal=" . $fila["codigo_sucursal"] . "&codigo_producto=" . $fila["codigo_producto"] . "' class='btn btn-danger btn-sm'>Eliminar</a></td>";

                        echo "</tr>";
                    }
                    ?>


                </tbody>
            </table>

        </section>










        <script src="assets/js/bootstrap.bundle.min.js" ></script>
        <script src="assets/js/jquery-3.6.0.min.js" ></script>


    </body>






</html>
