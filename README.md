# Modelo Vista Controlador en PHP y MySQL
Patrón de diseño MVC en PHP. Código generado para la asignatura Framework y programción web.

### Descripción
CRUD de control de stock en PHP, MVC, MySQL y Bootstrap.

## Frameworks utilizados
* [Bootstrap](http://getbootstrap.com/) - Biblioteca Bootstrap para diseño frontend

## Configuración
Abra config/database.php e ingrese los datos de configuración de su base de datos.

## Autores
*Jean Carlos Faundez* - *Luis Pezoa Ramos* - *Andrea Bravo Lopez*

