<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Control de stock</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" >
        <link href="assets/css/style.css" rel="stylesheet" >



    </head>
    <body>
        <section class="container" id="container">

            <div class="fx space-between gap20">
                <div>
                    <button id="atras" name="Dashboard" class="btn btn-light" onclick="window.location.href = 'index.php?c=dashboard&a=index'" >Dashboard</a>  
                </div>
                <h3><?php echo $data["Titulo"]; ?> </h3>

                <div class="fx align-items">
                    <a class="btn btn-primary" href="index.php?c=sucursal&a=nuevo">Nueva sucursal</a>
                </div>

            </div>
            
            <form method="POST" action="index.php?c=sucursal&a=busqueda" >
                
                   <div style="display: flex;justify-content: space-between;">
                    <div style="width: 316px;margin-right: 20px;">
                        <select class="form-select" aria-label="Default select example" id="cmbFiltro" name="cmbFiltro">
                            <option value="" >Filtro</option>
                            <option value="codigo_sucursal">Código de sucursal</option>
                            <option selected value="sucursal">Nombre de sucursal</option>

                        </select>
                    </div>

                    <!--Busqueda básica (1 solo filtro)-->
                    <div id="btnBasico" style="margin-right: 20px;display: flex;width: 95%;"> 
                        <input style="margin-right: 20px;" type="text" class="form-control" id="txtBusqueda" name="txtBusqueda" placeholder="Búsqueda básica" >
                        <button id="guardar" name="busquedaBasica" class="btn btn-primary" type="submit">Búsqueda</button>
                    </div>

                    <div id="btnAvanzado" style="width: 255px;">
                        <button class="btn btn-secondary" type="button" data-bs-toggle="collapse" data-bs-target=".multi-collapse1" aria-expanded="false" aria-controls="">Busqueda avanzada</button>
                    </div>
                </div>
                
                
            </form>


            <table class="table table table-striped" style="margin-top: 20px">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Código</th>
                        <th>Nombre</th>
                        <th>Modificar</th>
                        <th>Eliminar</th>

                    </tr>
                </thead>
                <tbody>

                    <?php
                    foreach ($data["sucursales"] as $fila) {
                        echo "<tr>";
                        echo "<td>" . $fila["id"] . "</td>";
                        echo "<td>" . $fila["codigo_sucursal"] . "</td>";
                        echo "<td>" . $fila["sucursal"] . "</td>";


                        echo "<td><a href='index.php?c=sucursal&a=modificar&id=" . $fila["id"] . "' class='btn btn-warning btn-sm'>Modificar</a></td>";
                        echo "<td><a href='index.php?c=sucursal&a=eliminar&id=" . $fila["id"] . "' class='btn btn-danger btn-sm'>Eliminar</a></td>";

                        echo "</tr>";
                    }
                    ?>


                </tbody>
            </table>

        </section>








        <script src="assets/js/bootstrap.bundle.min.js" ></script>
        <script src="assets/js/jquery-3.6.0.min.js" ></script>




    </body>






</html>
