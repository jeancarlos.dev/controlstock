<?php

class UserController {

    public function index() {
        require_once "views/Auth/login.php";
    }

    public function indexRegistrar() {
        $data["Titulo"] = "Registro de usuarios";

        require_once "views/Auth/Register.php";
    }

    public function registrarUsuario() {
        $correo = $_POST['correo'];
        $clave = $_POST['clave'];
        require_once "models/UserModel.php";
        $usuario = new UserModel();
        $usuario->insertar($correo, $clave);
        $data["Titulo"] = "Registro de usuarios";
        $this->indexRegistrar();
    }

    public function indexDashboard() {
        $data["Titulo"] = "Dashboard";
        require_once "views/dashboard/dashboard.php";
    }

    public function autenticar() {
        $email = $_POST['email'];
        $clave = $_POST['clave'];





        require_once "models/UserModel.php";
        $usuario = new UserModel();

        $data["usuario"] = $usuario->findByEmail($email);

        if ($data["usuario"] != null) {
            
            echo "existe registro en base de datos";
            
            $bdEmail = $data["usuario"]['email'];
            $bdClave = $data["usuario"]['clave'];

            if ($bdEmail == $email && $bdClave != $clave) {
                echo "Usuario registrado, pero clave incorrecta";
            }

            if ($bdEmail == $email && $bdClave == $clave) {
                echo "Usuario registrado, clave correcta";

                //SI el usuario existe crea una sesion.
                session_start();
                
                $_SESSION['miSesion']= array();
                $_SESSION['miSesion'][0] = $email;
                $_SESSION['miSesion'][1] = $clave;
      
                $this->indexDashboard();
            }
        } else {
            echo "Usuario no existe!";
        }
    }

}
