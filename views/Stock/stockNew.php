<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Control de stock</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" >
        <link href="assets/css/style.css" rel="stylesheet" >


    </head>
    <body>



        <section class="container gap20" id="container">




            <div class="card bg">
                <div class="card-header">
                    <h3><?php echo $data["Titulo"]; ?> </h3>
                </div>
                <div class="card-body">

                    <form id="nuevo" name="nuevo" method="POST" action="index.php?c=stock&a=guarda" autocomplete="off">



                     




                        <?php
                        include "controllers/SucursalController.php";
                        include "models/SucursalModel.php";
                        $sucursales = new SucursalModel();
                        $ejecutar = $sucursales->findAll();
                        ?>
                        <div class="mb-3">
                            <label class="form-label">Seleccione sucursal a cual asignara</label>
                            <select class="form-select" id="sucursal" name="codigo_sucursal" aria-label="Default select example" required>
                                <?php foreach ($ejecutar as $opciones): ?>
                                    <option value="<?php echo $opciones['codigo_sucursal'] ?>"><?php echo $opciones['codigo_sucursal'] ?> | <?php echo $opciones['sucursal'] ?></option>
                                <?php endforeach ?>
                            </select>

                        </div>
                        
                           <?php
                        //include "controllers/ProductController.php";
                        include "models/ProductModel.php";
                        $productos = new ProductModel();
                        $ejecutar = $productos->findAll();
                        ?>
                        <div class="mb-3">
                            <label class="form-label">Seleccione el código de producto</label>
                            <select class="form-select" id="codigo_producto" name="codigo_producto" aria-label="Default select example" required>
                                <?php foreach ($ejecutar as $opciones): ?>
                                    <option value="<?php echo $opciones['codigo_producto'] ?>"><?php echo $opciones['codigo_producto'] ?> | <?php echo $opciones['nombre'] ?></option>
                                <?php endforeach ?>
                            </select>

                        </div>



                        <div class="mb-3">
                            <label class="form-label">Stock actual</label>
                            <input type="number" class="form-control" id="stock_actual" name="stock_actual" placeholder="EJ: 1"  />
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Stock mínimo</label>
                            <input type="number" class="form-control" id="stock_minimo" name="stock_minimo" placeholder="EJ: 1"  />
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Stock máximo</label>
                            <input type="number" class="form-control" id="stock_maximo" name="stock_maximo" placeholder="EJ: 1"  />
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Precio</label>
                            <input type="number" class="form-control" id="precio" name="precio" placeholder="EJ: 15000"  />
                        </div>



                        <div class="fx gap-right">



                            <a id="atras" name="atras" class="btn btn-secondary" href="index.php?c=stock&a=index" >Atrás</a>
                            <button id="guardar" name="guardar" class="btn btn-primary" type="submit">Asignar stock</button>


                        </div>  

                    </form>




                </div>
            </div>

        </section>




    </body>
    <script src="assets/js/bootstrap.bundle.min.js" ></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
</html>
