<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Control de stock</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" >
        <link href="assets/css/style.css" rel="stylesheet" >


    </head>
    <body>



        <section class="container gap20" id="container">




            <div class="card bg">
                <div class="card-header">
                    <h3><?php echo $data["Titulo"]; ?> </h3>
                </div>
                <div class="card-body">

                    <form id="nuevo" name="nuevo" method="POST" action="index.php?c=product&a=guarda" autocomplete="off">

                        <div class="mb-3">
                            <label class="form-label">Código de producto</label>
                            <input type="text" class="form-control" id="codigo_producto" name="codigo_producto" placeholder="Ej: CIISA001" required>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="EJ: Notebook" required>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Categoria</label>
                            <select class="form-select" id="categoria" name="categoria" aria-label="Default select example"required>
                                <option selected>Seleccione</option>
                                <option value="escolares">Escolares</option>
                                <option value="telefonia">Telefonía</option>
                                <option value="tecnologia">Tecnología</option>
                                <option value="electro">Electro</option>
                                <option value="decohogar">Decohogar</option>
                                <option value="muebles">Muebles</option>
                                <option value="dormitorios">Dormitorio</option>
                                <option value="deportes">Deportes</option>
                                <option value="mujer">Mujer</option>
                                <option value="hombre">Hombre</option>
                                <option value="niños">Niños y jugueteria</option>
                                <option value="bebes">Bebes</option>
                                <option value="zapatos">Zapatos</option>
                                <option value="belleza">Belleza</option>
                                <option value="accesorios">Accesorios</option>
                            </select>
                        </div>



<!--                        <?php
                        include "controllers/SucursalController.php";
                        include "models/SucursalModel.php";
                        $sucursales = new SucursalModel();
                        $ejecutar = $sucursales->findAll();
                        ?>
                        <div class="mb-3">
                            <label class="form-label">Sucursal</label>
                            <select class="form-select" id="codigo_sucursal" name="codigo_sucursal" aria-label="Default select example" required>
                                <?php foreach ($ejecutar as $opciones): ?>
                                    <option id="<?php echo $opciones['codigo_sucursal'] ?>" name="<?php echo $opciones['codigo_sucursal'] ?>" value="<?php echo $opciones['codigo_sucursal'] ?>"><?php echo $opciones['sucursal'] ?></option>
                                        <?php endforeach ?>
                            </select>

                        </div>-->


                        <div class="mb-3">
                            <label class="form-label">Descripción</label>
                            <textarea type="textarea" class="form-control" id="descripcion" name="descripcion" placeholder="Descripción"></textarea>
                        </div>

<!--                        <div class="mb-3">
                            <label for="formGroupExampleInput" class="form-label">Stock acutal</label>
                            <input type="number" class="form-control" id="stock_actual" name="stock_actual" placeholder="Ej:1" required>
                        </div>

                        <div class="mb-3">
                            <label for="formGroupExampleInput" class="form-label">Precio</label>
                            <input type="number" class="form-control" id="precio" name="precio" placeholder="Ej:1" required>
                        </div>-->





                        <div class="fx gap-right">

                            <button id="atras" name="atras" class="btn btn-secondary" onclick="window.location.href = 'index.php?c=product&a=index'" >Atrás</a>
                                <button id="guardar" name="guardar" class="btn btn-primary" type="submit">Guardar producto</button>


                        </div>  

                    </form>




                </div>
            </div>

        </section>

   <script src="assets/js/bootstrap.bundle.min.js" ></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>


    </body>
 
</html>
