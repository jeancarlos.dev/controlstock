<?php

/**
 * @author Jean Carlos
 */

/**
 * Metodo que carga el controlador dependiendo el controlador que se pase por parametro, este parametro vendra desde la URL
 * @param type $controlador es el que se obtiene desde la url por medio del verbo GET.
 * @param type $nombreControlador, 
 */
function cargarControlador($controlador) {

    /* Se capitaliza el nombre del controlador que se obtenga por parametro, luego se concadena la palabra 'Controller' y se asigna a la variable $nombreControlador
      Ejemplo: $nombreControlador = productoController.php o personaController.php
     */

    $nombreControlador = ucwords($controlador) . "Controller";
    $archivoControlador = 'controllers/' . ucwords($controlador."Controller") . '.php';

    //Si no existe el archivo, cargara el controlador por defecto.
    if (!is_file($archivoControlador)) {
        $archivoControlador = 'controllers/' . CONTROLADOR_PRINCIPAL . '.php';
        
        //echo "no existe seteando principal: ".$archivoControlador;
    }else{
        //echo "existe el archivo: ".$archivoControlador;
    }

    require_once $archivoControlador;
    
    $control = new $nombreControlador();
    return $control;
}

/**
 * Metodo que carga la accion dependiendo la accion que se pase por parametro, este parametro vendra desde la URL
 * @param type $controller si el controlador existe como archivo previamente entonces se pasara como parametro.
 * @param type $accion, 
 */
function cargarAccion($controller, $accion,$id=null) {
    if (isset($accion) && method_exists($controller, $accion)) {
        if ($id == null) {
            $controller->$accion();
        } else {
            $controller->$accion($id);
        }
    } else {
        $controller->ACCION_PRINCIPAL();
    }
}

?>